## CS5024 Assignment 2 - Chain Reaction

Team 1C

- Cormac Bracken  9210318
- John O’Sullivan 0460842
- Fearghal Sheehan 8907943

This is our submission for the DimeCR Chain Reaction assignment.

Please run the Strategy_Group1C.process file. This process uses sub-processes in the same folder.

Also, please note that the strategy was performing much better a few days ago. We made an effort to refactor the process into sub-processes and that seems to have impacted the performance. We ran out of time to figure out why.

Thanks!

Cormac, John and Fearghal.